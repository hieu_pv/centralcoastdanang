<?php
require __DIR__ . '/vendor/autoload.php';
define('THEME_URI', get_template_directory_uri());
require_once __DIR__ . '/include/post_query.php';
function register_my_menu()
{
    register_nav_menu('header-menu', __('Header Menu'));
}
add_action('init', 'register_my_menu');
$args = array(
    'default-image' => get_template_directory_uri() . '/images/logo.png',
);
add_theme_support('custom-header', $args);

add_theme_support('post-thumbnails');

/**
 * Filters wp_title to print a neat <title> tag based on what is being viewed.
 *
 * @param string $title Default title text for current view.
 * @param string $sep Optional separator.
 * @return string The filtered title.
 */
function theme_name_wp_title($title, $sep)
{
    if (is_feed()) {
        return $title;
    }

    global $page, $paged;

    // Add the blog name
    $title .= get_bloginfo('name', 'display');

    // Add the blog description for the home/front page.
    $site_description = get_bloginfo('description', 'display');
    if ($site_description && (is_home() || is_front_page())) {
        $title .= " $sep $site_description";
    }

    // Add a page number if necessary:
    if (($paged >= 2 || $page >= 2) && !is_404()) {
        $title .= " $sep " . sprintf(__('Page %s', '_s'), max($paged, $page));
    }

    return $title;
}
add_filter('wp_title', 'theme_name_wp_title', 10, 2);
/*
 * register widget
 */
function regist_footer_widget()
{

    $widgets = array(
        array(
            'name' => 'Index Text Widget',
            'description' => 'Tên dự án - trang chủ',
            'before' => '<h1 class="widget-title">',
            'after' => '</h1>',
        ),
        array(
            'name' => 'Project Info',
            'description' => 'Thông tin dự án',
            'before' => '<h3 class="widget-title">',
            'after' => '</h3>',
        ),
        array(
            'name' => 'NEWS',
            'description' => 'Tin Tức',
            'before' => '<h3 class="widget-title">',
            'after' => '</h3>',
        ),
        array(
            'name' => 'Contact',
            'description' => 'Thông tin liên hệ',
            'before' => '<h3 class="widget-title">',
            'after' => '</h3>',
        ),
        array(
            'name' => 'Contact Title',
            'description' => 'Tiêu đề - thông tin liên hệ',
            'before' => '<h3 class="widget-title">',
            'after' => '</h3>',
        ),
        array(
            'name' => 'Footer 1',
            'description' => 'Tiêu đề - thông tin liên hệ',
            'before' => '<h3 class="widget-title">',
            'after' => '</h3>',
        ),
        array(
            'name' => 'Footer 2',
            'description' => 'Tiêu đề - thông tin liên hệ',
            'before' => '<h3 class="widget-title">',
            'after' => '</h3>',
        ),
        array(
            'name' => 'Footer 3',
            'description' => 'Tiêu đề - thông tin liên hệ',
            'before' => '<h3 class="widget-title">',
            'after' => '</h3>',
        ),
        array(
            'name' => 'Footer 4',
            'description' => 'Footer - tìm kiếm',
            'before' => '<h3 class="widget-title">',
            'after' => '</h3>',
        ),
        array(
            'name' => 'Recent Posts',
            'description' => 'Sidebar - bài viết mới nhất',
            'before' => '<h3 class="widget-title">',
            'after' => '</h3>',
        ),
        array(
            'name' => 'Apartment Design',
            'description' => 'Sidebar - thiết kế căn hộ',
            'before' => '<h3 class="widget-title">',
            'after' => '</h3>',
        ),
        array(
            'name' => 'Recent Posts Bottom',
            'description' => 'Bottom - Bài viết liên quan',
            'before' => '<h3 class="widget-title">',
            'after' => '</h3>',
        ),
        array(
            'name' => 'Tai Sao Chon San Dat Vang',
            'description' => 'Tại sao chọn sàn đất vàng',
            'before' => '<h3 class="widget-title">',
            'after' => '</h3>',
        ),
        array(
            'name' => 'Tieu Chi Cua San Dat Vang',
            'description' => 'Tiêu chí của sàn đất vàng',
            'before' => '<h3 class="widget-title">',
            'after' => '</h3>',
        ),
        array(
            'name' => 'Khach Hang Noi Ve San Dat Vang',
            'description' => 'Khách hàng nói về sàn đất vàng',
            'before' => '<h3 class="widget-title">',
            'after' => '</h3>',
        ),
        array(
            'name' => 'Tin Tức Bất động sản',
            'description' => 'Tin tức bất động sản',
            'before' => '<h3 class="widget-title">',
            'after' => '</h3>',
        ),
        array(
            'name' => 'Google maps',
            'description' => 'Google Maps',
            'before' => '<h3 class="widget-title">',
            'after' => '</h3>',
        ),
        array(
            'name' => 'Tieu De Block Du An',
            'description' => 'Tiều để của block các dự án bất động sản',
            'before' => '<h3 class="widget-title">',
            'after' => '</h3>',
        ),
        array(
            'name' => 'Subiz Chat',
            'description' => 'chèn mã nhúng subiz',
            'before' => '<h3 class="widget-title">',
            'after' => '</h3>',
        ),
    );
    foreach ($widgets as $key => $widget) {
        register_sidebar(array(
            'name' => $widget['name'],
            'id' => strtolower(str_replace(" ", "-", $widget['name'])),
            'description' => $widget['description'],
            'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
            'after_widget' => '</div>',
            'before_title' => $widget['before'],
            'after_title' => $widget['after'],
        ));
    }
}

add_action('widgets_init', 'regist_footer_widget');
/*
 * add post type
 */
add_action('init', 'create_products_post_type');
function create_products_post_type()
{
    register_post_type('products',
        array(
            'labels' => array(
                'name' => __('Thông tin dự án'),
                'singular_name' => __('project'),
                'all_items' => 'Tất cả',
                'add_new' => 'Thêm',
            ),
            'rewrite' => array('slug' => 'thong-tin-du-an'),
            'public' => true,
            'has_archive' => false,
            'query_var' => 'project',
            'taxonomies' => array(''),
            'supports' => ['title', 'thumbnail', 'editor'],
        )
    );
}

add_action('init', 'register_post_type_design');
function register_post_type_design()
{
    register_post_type('design',
        array(
            'labels' => array(
                'name' => __('Thiết kế căn hộ'),
                'singular_name' => __('design'),
                'all_items' => 'Tất cả',
                'add_new' => 'Thêm',
            ),
            'rewrite' => array('slug' => 'thiet-ke'),
            'public' => true,
            'has_archive' => false,
            'query_var' => 'design',
            'taxonomies' => array(''),
            'supports' => ['title', 'thumbnail', 'editor'],
        )
    );
}
add_action('init', 'register_post_type_why_is_goldland');
function register_post_type_why_is_goldland()
{
    register_post_type('reason',
        array(
            'labels' => array(
                'name' => __('Tại sao chọn sandatvang'),
                'singular_name' => __('reason'),
                'all_items' => 'Tất cả',
                'add_new' => 'Thêm',
            ),
            'rewrite' => array('slug' => 'tai-sao-chon-san-dat-vang'),
            'public' => true,
            'has_archive' => false,
            'query_var' => 'reason',
            'taxonomies' => array(''),
            'supports' => ['title', 'thumbnail', 'editor'],
        )
    );
}
add_action('init', 'register_post_type_targer_goldland');
function register_post_type_targer_goldland()
{
    register_post_type('tieu-chi',
        array(
            'labels' => array(
                'name' => __('Tiêu chí của sandatvang'),
                'singular_name' => __('tieu-chi'),
                'all_items' => 'Tất cả',
                'add_new' => 'Thêm',
            ),
            'rewrite' => array('slug' => 'tieu-chi'),
            'public' => true,
            'has_archive' => false,
            'query_var' => 'tieu-chi',
            'taxonomies' => array(''),
            'supports' => ['title', 'thumbnail', 'editor'],
        )
    );
}
add_action('init', 'register_post_type_feedback');
function register_post_type_feedback()
{
    register_post_type('feedback',
        array(
            'labels' => array(
                'name' => __('Khách hàng nói về sandatvang'),
                'singular_name' => __('khach-hang-noi-ve-san-dat-vang'),
                'all_items' => 'Tất cả',
                'add_new' => 'Thêm',
            ),
            'rewrite' => array('slug' => 'khach-hang-noi-ve-san-dat-vang'),
            'public' => true,
            'has_archive' => false,
            'query_var' => 'feedback',
            'taxonomies' => array(''),
            'supports' => ['title', 'thumbnail', 'editor'],
        )
    );
}
add_action('init', 'register_post_type_tin_bds');
function register_post_type_tin_bds()
{
    register_post_type('tin-bds',
        array(
            'labels' => array(
                'name' => __('Tin Tức'),
                'singular_name' => __('tin-tuc-bds'),
                'all_items' => 'Tất cả',
                'add_new' => 'Thêm',
            ),
            'rewrite' => array('slug' => 'tin-tuc-bds'),
            'public' => true,
            'has_archive' => false,
            'query_var' => 'tin-bds',
            'taxonomies' => array(''),
            'supports' => ['title', 'thumbnail', 'editor'],
        )
    );
}

add_action('init', 'register_post_du_an');
function register_post_du_an()
{
    register_post_type('du-an',
        array(
            'labels' => array(
                'name' => __('Các Dự Án'),
                'singular_name' => __('du-an'),
                'all_items' => 'Tất cả',
                'add_new' => 'Thêm',
            ),
            'rewrite' => array('slug' => 'du-an'),
            'public' => true,
            'has_archive' => false,
            'query_var' => 'du-an',
            'taxonomies' => array(''),
            'supports' => ['title', 'thumbnail', 'editor'],
        )
    );
}

function get_permalink_by_slug($slug)
{
    $obj = get_page_by_path($slug);
    return get_permalink($obj->ID);
}

function posts_list_function($attrs)
{
    if (isset($_GET['page'])) {
        $paged = $_GET['page'];
    } else {
        $paged = 1;
    }
    $args = [
        'post_type' => $attrs['type'],
        'posts_per_page' => 20,
        'paged' => $paged,
    ];
    $cate_posts = new WP_Query($args);
    while ($cate_posts->have_posts()): $cate_posts->the_post()?>
		<div class="search-result col-md-4 col-sm-6 col-xs-12">
			<div class="image">
				<a href="<?php the_permalink();?>" title="<?php the_title();?>">
					<?php echo get_the_post_thumbnail($post->ID, 'thumbnail');?>
				</a>
			</div>
			<div class="p_row">
				<div class="post-title">
					<a href="<?php the_permalink();?>" title="<?php the_title();?>">
						<p><?php the_title();?></p>
					</a>
				</div>
			</div>
		</div>
		<?php endwhile;?>
	<div class="col-md-12 col-sm-12 col-xs-12">
		<?php if (function_exists('NightFuryBootstrapPagination')) {
        NightFuryBootstrapPagination($cate_posts);
    }
    ?>
	</div>
	<?php
}
add_shortcode('posts_list', 'posts_list_function');
?>
