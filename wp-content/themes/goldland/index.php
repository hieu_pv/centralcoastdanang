<?php get_header()?>
	<div class="container">
		<div id="text-widget" class="text-center animated fadeInDown col-md-12 col-xs-12 col-sm-12"><?php dynamic_sidebar('index-text-widget')?></div>
		<div id="slider" class="col-md-12 col-xs-12 col-sm-12"><?php masterslider(1)?></div>


		<div id="project-info" class="paralax invisible text-center group col-md-12 col-xs-12 col-sm-12">
			<div class="title"><?php dynamic_sidebar('project-info')?></div>
			<div class="list" id="project-slick">
				<?php
					$posts = project_info();
					while ($posts->have_posts()): $posts->the_post()
				?>
				<div class="project">
					<div class="image">
						<a href="<?php the_permalink();?>" title="<?php the_title();?>">
							<?php echo get_the_post_thumbnail($post->ID, 'thumbnail');?>
						</a>
					</div>
					<div class="p_row">
						<!-- <div class="time">
							<div class="date"><?php echo '<span>' . get_the_date('j', $post->ID) . '</span><span>' . get_the_date('F', $post->ID) . '</span>';?></div>
						</div> -->
						<div class="post-title">
							<a href="<?php the_permalink();?>" title="<?php the_title();?>">
								<p><?php the_title();?></p>
							</a>
						</div>
					</div>
				</div>
				<?php endwhile?>
			</div>
		</div>


		<div id="news" class="paralax invisible text-center group col-md-12 col-xs-12 col-sm-12">
			<div class="title"><?php dynamic_sidebar('news')?></div>
			<div class="list" id="news-slick">
				<?php
					$posts = news_query();
					while ($posts->have_posts()): $posts->the_post()
				?>
					<div class="news">
						<div class="image">
							<a href="<?php the_permalink();?>" title="<?php the_title();?>">
								<?php echo get_the_post_thumbnail($post->ID, 'thumbnail');?>
							</a>
						</div>
						<div class="p_row">
							<!-- <div class="time">
								<div class="date"><?php echo '<span>' . get_the_date('j', $post->ID) . '</span><span>' . get_the_date('F', $post->ID) . '</span>';?></div>
							</div> -->
							<div class="post-title">
								<a href="<?php the_permalink();?>" title="<?php the_title();?>">
									<p><?php the_title();?></p>
								</a>
							</div>
						</div>
					</div>
				<?php endwhile?>
			</div>
		</div>

		<div class="col-md-12 col-xs-12 col-sm-12">
			<div class="col-md-6 col-sm-6 col-xs-12">
				<div id="why-is-goldland" class="paralax invisible"><?php dynamic_sidebar('tai-sao-chon-san-dat-vang');?></div>
				<div id="tin-bds" class="paralax invisible"><?php dynamic_sidebar('tin-tuc-bat-dong-san');?></div>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12">
				<div id="target" class="paralax invisible"><?php dynamic_sidebar('tieu-chi-cua-san-dat-vang');?></div>
				<div id="feedback" class="paralax invisible"><?php dynamic_sidebar('khach-hang-noi-ve-san-dat-vang');?></div>
			</div>
		</div>
		<div id="du-an-bds" class="paralax invisible group col-md-12 col-xs-12 col-sm-12">
			<div class="title text-center"><?php dynamic_sidebar('tieu-de-block-du-an')?></div>
			<div class="du-an" id="du-an-slick">
				<?php
					$posts = du_an_query();
					while ($posts->have_posts()): $posts->the_post()
				?>
					<div class="du-an-item">
						<div class="image">
							<a href="<?php the_permalink();?>" title="<?php the_title();?>">
								<?php echo get_the_post_thumbnail($post->ID, 'thumbnail');?>
							</a>
						</div>
						<div class="p_row">
							<div class="post-title">
								<a href="<?php the_permalink();?>" title="<?php the_title();?>">
									<p><?php the_title();?></p>
								</a>
							</div>
						</div>
					</div>
				<?php endwhile?>
			</div>
		</div>
		<div id="contact" class="paralax invisible group col-md-12 col-xs-12 col-sm-12">
			<div class="title text-center"><?php dynamic_sidebar('contact-title')?></div>
			<div id="contact-widget"><?php dynamic_sidebar('contact');?></div>
		</div>
	</div>

	<script src="<?php echo THEME_URI;?>/../../plugins/master-slider/public/assets/js/masterslider.min.js"></script>
	<link rel="stylesheet" href="<?php echo THEME_URI;?>/../../plugins/master-slider/public/assets/css/masterslider.main.min.css">
	<script src="<?php echo THEME_URI;?>/bower_components/slick.js/slick/slick.min.js"></script>
	<link rel="stylesheet" href="<?php echo THEME_URI;?>/bower_components/slick.js/slick/slick.css">
	<script src="<?php echo THEME_URI;?>/js/goldland.js"></script>
<?php get_footer()?>