<?php get_header();?>
	<div class="container">
		<div class="col-md-8 col-xs-12 col-sm-7">
			<?php 
				if(isset($_GET['page'])) {
					$paged = $_GET['page'];
				} else {
					$paged = 1;
				}
				$cate_posts = category_query($cat, $paged);
				while ($cate_posts->have_posts()): $cate_posts->the_post()?>
				<div class="search-result col-md-4 col-sm-6 col-xs-12">
					<div class="image">
						<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
							<?php echo get_the_post_thumbnail($post->ID,'thumbnail'); ?>
						</a>
					</div>
					<div class="p_row">
						<div class="post-title">
							<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
								<p><?php the_title(); ?></p>
							</a>
						</div>
					</div>
				</div>
			<?php endwhile;?>
			<div class="col-md-12 col-sm-12 col-xs-12">
				<?php if(function_exists('NightFuryBootstrapPagination')) {
					NightFuryBootstrapPagination($cate_posts);
				} ?>
			</div>
		</div>
		<div class="col-md-4 col-sm-5 col-xs-12">
		<?php get_search_form(); ?>
			<?php dynamic_sidebar('recent-posts'); ?>
			<?php dynamic_sidebar('apartment-design'); ?>
		</div>
		<div id="contact" class="paralax invisible group col-md-12 col-xs-12 col-sm-12">
			<div class="title text-center"><?php dynamic_sidebar('contact-title')?></div>
			<div id="contact-widget"><?php dynamic_sidebar('contact');?></div>
		</div>
	</div>
	<script src="<?php echo THEME_URI;?>/bower_components/slick.js/slick/slick.min.js"></script>
	<link rel="stylesheet" href="<?php echo THEME_URI;?>/bower_components/slick.js/slick/slick.css">
	<script src="<?php echo THEME_URI;?>/js/goldland.js"></script>
<?php get_footer();?>