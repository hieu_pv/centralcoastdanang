<!DOCTYPE html>
<html lang="en">
<head>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-85700147-1', 'auto');
  ga('send', 'pageview');

</script>
    <?php wp_head(); ?>
    <meta charset="UTF-8">
    <title><?php wp_title(); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="<?php echo THEME_URI; ?>/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="<?php echo THEME_URI; ?>/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="<?php echo THEME_URI; ?>/bower_components/bootstrap/dist/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="<?php echo THEME_URI; ?>/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo THEME_URI; ?>/bower_components/animate.css/animate.min.css">
    <link rel="stylesheet" href="<?php echo THEME_URI; ?>/css/goldland.css">
</head>
<body>
    <header>
        <nav class="navbar navbar-default navbar-fixed-top">
          <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="<?php echo bloginfo('siteurl'); ?>"><img src="<?php echo header_image(); ?>" alt="<?php wp_title(); ?>"></a>
            </div>
            <?php wp_nav_menu(['theme_location'  => 'header-menu','menu_id' => 'main-menu']); ?>
          </div><!-- /.container -->
        </nav>
    </header>
    <div class="wrapper">
        
