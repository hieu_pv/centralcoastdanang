<?php
/**
 * All Variables here
 */
class NfVariables
{
    public static $defaut_posts_per_page = 20;
    public static $default_post_paged = 1;
    public static $default_post_order = 'rand';
}
