<?php
/**
 * All WP Query will defined here
 */
class NfQuery extends \Exception
{

    public function __construct()
    {

    }
    public function project_info()
    {
        $args = [
            'post_type' => 'products',
            'posts_per_page' => 20,
            'paged' => 1,
            'orderby' => 'rand',
        ];
        return new WP_Query($args);
    }

    public function news_query()
    {
        $args = [
            'post_type' => 'post',
            'posts_per_page' => 20,
            'paged' => 1,
            'orderby' => 'rand',
        ];
        return new WP_Query($args);
    }
    public function du_an_query()
    {
        $args = [
            'post_type' => 'du-an',
            'posts_per_page' => 20,
            'paged' => 1,
            'orderby' => 'rand',
        ];
        return new WP_Query($args);
    }
    public function category_query($cat, $paged)
    {
        $args = [
            'cat' => $cat,
            'posts_per_page' => 18,
            'paged' => $paged,
        ];
        return new WP_Query($args);
    }
}
