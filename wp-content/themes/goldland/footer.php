		</div>
		<footer>
			<div class="container">
				<div class="col-md-4 col-sm-4 col-xs-12 paralax invisible" id="footer-1">
					<?php dynamic_sidebar("footer-1");?>
					<?php dynamic_sidebar("google-maps");?>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 paralax invisible" id="footer-2">
					<?php dynamic_sidebar("footer-4");?>
					<?php dynamic_sidebar("footer-2");?>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 paralax invisible" id="footer-3">
					<?php dynamic_sidebar("footer-3");?>
				</div>
			</div>
		</footer>
		<?php dynamic_sidebar('subiz-chat');?>
		<?php wp_footer();?>
	</body>
</html>