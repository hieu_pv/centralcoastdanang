<?php get_header();?>
	<div id="fb-root"></div>
	<script>
	(function(d, s, id) {
	    var js, fjs = d.getElementsByTagName(s)[0];
	    if (d.getElementById(id)) return;
	    js = d.createElement(s);
	    js.id = id;
	    js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=864606800234299";
	    fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	</script>

	<div class="container">
		<div id="goldland-single">
			<div id="breadcrumbs" class="col-md-12 col-xs-12 col-sm-12">
				<ol class="breadcrumb">
				  <li><a href="<?php echo bloginfo('siteurl');?>">Trang chủ</a></li>
				  <li class="active"><?php the_title();?></li>
				</ol>
			</div>
			<div class="col-sm-4 col-md-8 col-xs-12 overflow-hidden">
				<?php while (have_posts()): the_post()?>
							<h1 class="page-title"><?php the_title();?></h1>
							<div class="content"><?php the_content();?></div>
						<?php endwhile;?>
				<div class="fb-comments" data-href="<?php the_permalink();?>" data-numposts="5"></div>
				<div class="recent-posts paralax invisible"><?php dynamic_sidebar('recent-posts-bottom');?></div>
			</div>
			<div class="col-md-4 col-sm-4 col-xs-12">
				<?php get_search_form();?>
				<?php dynamic_sidebar('recent-posts');?>
				<?php dynamic_sidebar('apartment-design');?>
			</div>
		</div>
		<div id="contact" class="paralax invisible group col-md-12 col-xs-12 col-sm-12">
			<div class="title text-center"><?php dynamic_sidebar('contact-title')?></div>
			<div id="contact-widget"><?php dynamic_sidebar('contact');?></div>
		</div>
	</div>
	<script src="<?php echo THEME_URI;?>/bower_components/slick.js/slick/slick.min.js"></script>
	<link rel="stylesheet" href="<?php echo THEME_URI;?>/bower_components/slick.js/slick/slick.css">
	<script src="<?php echo THEME_URI;?>/js/goldland.js"></script>
<?php get_footer();?>