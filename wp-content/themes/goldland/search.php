<?php get_header();?>
	<div class="container">
		<div class="col-md-12 col-sm-12 col-xs-12"><h1 class="search-result-title">Kết quả tìm kiếm:</h1></div>
		<div class="col-md-8 col-xs-12 col-sm-7">
			<?php while (have_posts()): the_post()?>
				<div class="search-result col-md-4 col-sm-6 col-xs-12">
					<div class="image">
						<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
							<?php echo get_the_post_thumbnail($post->ID,'thumbnail'); ?>
						</a>
					</div>
					<div class="p_row">
						<div class="post-title">
							<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
								<p><?php the_title(); ?></p>
							</a>
						</div>
					</div>
				</div>
			<?php endwhile;?>
		</div>
		<div class="col-md-4 col-sm-5 col-xs-12">
		<?php get_search_form(); ?>
			<?php dynamic_sidebar('recent-posts'); ?>
			<?php dynamic_sidebar('apartment-design'); ?>
		</div>
	</div>
<?php get_footer();?>