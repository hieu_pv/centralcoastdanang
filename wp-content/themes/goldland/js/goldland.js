$(document).ready(function() {
    var ProjectInfo = $('#project-slick');
    var Slicks = [{
        element: $('#project-slick'),
        autoplay: true,
        slideNumber: {
            md: 3,
            sm: 3,
            xs: 1,
        },
    }, {
        element: $('#news-slick'),
        autoplay: true,
        slideNumber: {
            md: 3,
            sm: 3,
            xs: 1,
        },
    }, {
        element: $('#contact-widget'),
        autoplay: true,
        slideNumber: {
            md: 5,
            sm: 3,
            xs: 1,
        },
    }, {
        element: $('#du-an-slick'),
        autoplay: true,
        slideNumber: {
            md: 5,
            sm: 3,
            xs: 1,
        },
    }, ];
    Slicks.forEach(function(value, key) {
        value.element.slick({
            slidesToShow: value.slideNumber.md,
            slidesToScroll: 1,
            autoplay: value.autoplay,
            autoplaySpeed: 5000,
            lazyLoad: 'ondemand',
            prevArrow: '<span class="glyphicon glyphicon-chevron-left"></span>',
            nextArrow: '<span class="glyphicon glyphicon-chevron-right"></span>',
            responsive: [{
                breakpoint: 992,
                settings: {
                    slidesToShow: value.slideNumber.sm,
                    slidesToScroll: 1
                }
            }, {
                breakpoint: 767,
                settings: {
                    slidesToShow: value.slideNumber.xs,
                    slidesToScroll: 1
                }
            }]
        });
    });
    $('#slider img:first-child').load(function() {
        scroll();
    })

    function scroll() {
        var wHeight = $(window).height();
        var wScroll = $(window).scrollTop();
        $('.paralax').each(function() {
            var elementTop = $(this).offset().top;
            if (wHeight + wScroll > elementTop + 100) {
                if ($(this).hasClass('invisible')) {
                    $(this).removeClass('invisible');
                    $(this).addClass('animated');
                    $(this).addClass('fadeInUp');
                }
            }
        })
    }
    $(window).scroll(function() {
        scroll();
    })
    $('.disable-link a').click(function(evt) {
        evt.preventDefault();
    });
    $(document).ready(function() {
        // auto reload after 10 minutes
        var AutoReload;
        var reloadTime = 60000 * 5;
        AutoReload = setTimeout(function() {
            document.location.reload();
        }, reloadTime);
        $(document.body).bind("mousemove keypress", function(e) {
            clearTimeout(AutoReload);
            AutoReload = AutoReload = setTimeout(function() {
                document.location.reload();
            }, reloadTime);
        });
        // random contact widget order
        var contacts = document.querySelector('#contact-widget .slick-track');
        for (var i = contacts.children.length; i >= 0; i--) {
            contacts.appendChild(contacts.children[Math.random() * i | 0]);
        };
    });
});
