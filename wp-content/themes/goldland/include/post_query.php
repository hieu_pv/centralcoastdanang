<?php

function project_info() {
	$args = [
		'post_type' => 'products',
		'posts_per_page' => 20,
		'paged' => 1,
		'orderby' => 'rand',
	];
	return new WP_Query($args);
}

function news_query() {
	$args = [
		'post_type' => 'post',
		'posts_per_page' => 20,
		'paged' => 1,
		'orderby' => 'rand',
	];
	return new WP_Query($args);
}
function du_an_query() {
	$args = [
		'post_type' => 'du-an',
		'posts_per_page' => 20,
		'paged' => 1,
		'orderby' => 'rand',
	];
	return new WP_Query($args);
}
function category_query($cat, $paged) {
	$args = [
		'cat' => $cat,
		'posts_per_page' => 18,
		'paged' => $paged,
	];
	return new WP_Query($args);
}
?>