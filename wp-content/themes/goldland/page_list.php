<?php get_header();?>
	<div class="container">
		<div class="col-md-8 col-xs-12 col-sm-7">
			<?php the_content(); ?>
		</div>
		<div class="col-md-4 col-sm-5 col-xs-12">
		<?php get_search_form(); ?>
			<?php dynamic_sidebar('recent-posts'); ?>
			<?php dynamic_sidebar('apartment-design'); ?>
		</div>
		<div id="contact" class="paralax invisible group col-md-12 col-xs-12 col-sm-12">
			<div class="title text-center"><?php dynamic_sidebar('contact-title')?></div>
			<div id="contact-widget"><?php dynamic_sidebar('contact');?></div>
		</div>
	</div>
	<script src="<?php echo THEME_URI;?>/bower_components/slick.js/slick/slick.min.js"></script>
	<link rel="stylesheet" href="<?php echo THEME_URI;?>/bower_components/slick.js/slick/slick.css">
	<script src="<?php echo THEME_URI;?>/js/goldland.js"></script>
<?php get_footer();?>