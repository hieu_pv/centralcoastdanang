<?php
function nightfury_bootstrap_menu_option_page(){
	// delete_option(NIGHTFURY_BOOTSTRAP_OPTION_KEY);
	$MenuTypes = array(
		'BootstrapAccordionMenuWalker' => 'Accordion Menu',
		'NightFuryBootstrapNavbar' => 'Boostrap Navbar',
	);

	$menus = get_registered_nav_menus();

	$NightFuryOption = get_option(NIGHTFURY_BOOTSTRAP_OPTION_KEY,'false');
	if(isset($_POST['submit']))
	{
		unset($_POST['submit']);
		if($NightFuryOption == 'false')
		{
			add_option(NIGHTFURY_BOOTSTRAP_OPTION_KEY,$_POST,'', 'yes');
		}
		else
		{
			update_option(NIGHTFURY_BOOTSTRAP_OPTION_KEY,$_POST,'', 'yes');
		}
	}
	
	$NightFuryOption = get_option(NIGHTFURY_BOOTSTRAP_OPTION_KEY,'false');
	// echo var_dump($NightFuryOption);
	?>

	<h4>Apply settings for</h4>
	<form method="post">

	<?php
		foreach($menus as $key => $value) {
	?>
		<div data="<?php echo $key; ?>">
			<!-- <input name="<?php echo $key; ?>" id="bootstrap-accordition-<?php echo $key; ?>"<?php echo in_array($key, $NightFuryOption) ? ' checked="true"' : ''; ?> type="checkbox" style="margin-right: 10px;"> -->
			<select name="<?php echo $key; ?>" id="bootstrap-accordition-<?php echo $key; ?>">
				<option value="">None</option>
				<?php foreach($MenuTypes as $menu_type_key => $menu_type){ ?>
					<option value="<?php echo $menu_type_key ?>"<?php echo ($NightFuryOption[$key] == $menu_type_key) ? ' selected="true"' : ''; ?>><?php echo $menu_type; ?></option>
				<?php } //end foreach ?> 
			</select>
			<span><?php echo $value; ?></span></div>

	<?php } ?>

	<?php submit_button(); ?>

	</form>
<?php
}
?>