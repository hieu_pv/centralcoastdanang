<?php
/*
Plugin Name: NightFury Bootstrap Pagination
Plugin URI:  http://wp.vietnamdev.com/plugins/nightfury-bootstrap-pagination
Description: This plugin will help you create an accordion menu.
Version:     1.0.0
Author:      Night Fury
Author URI:  http://wp.vietnamdev.com/plugins/nightfury-bootstrap-pagination
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Domain Path: http://wp.vietnamdev.com/plugins/nightfury-bootstrap-pagination
Text Domain: http://wp.vietnamdev.com/plugins/nightfury-bootstrap-pagination
 */

/*
 * Bootstrap Pagination
 */

function NightFuryBootstrapPagination($wp_query) {
	if ((int) $wp_query->max_num_pages <= 1) {
		return false;
	} else {
		$page_pattern = "/(page=)([0-9]+)/i";
		$current_url = 'http' . (isset($_SERVER['HTTPS']) ? 's' : '') . "://$_SERVER[HTTP_HOST]/$_SERVER[REQUEST_URI]";
		if (preg_match($page_pattern, $current_url)) {
			if ($wp_query->query_vars['paged'] <= 1) {
				$prev_url = preg_replace($page_pattern, '${1}1', $current_url);
			} else {
				$prev_url = preg_replace($page_pattern, '${1}' . ($wp_query->query_vars['paged'] - 1), $current_url);
			}
			if ($wp_query->query_vars['paged'] >= (int) $wp_query->max_num_pages) {
				$next_url = preg_replace($page_pattern, '${1}' . $wp_query->max_num_pages, $current_url);
			} else {
				$next_url = preg_replace($page_pattern, '${1}' . ($wp_query->query_vars['paged'] + 1), $current_url);
			}
		} else {
			$prev_url = $current_url;
			$next_url = $current_url . '?page=2';
		}
		$output = '<div class="bootstrap-pagination"><ul class="pagination"><li><a href="' . $prev_url . '" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>';

		$total_page = (int) $wp_query->max_num_pages;
		for ($i = 1; $i <= $total_page; $i++) {
			if (preg_match($page_pattern, $current_url)) {
				$replacement = '${1}' . $i;
				$page_url = preg_replace($page_pattern, $replacement, $current_url);
			} else {
				$page_url = $current_url . '?page=' . $i;
			}
			$output .= '<li><a href="' . $page_url . '">' . $i . '</a></li>';
		}

		$output .= '<li><a href="' . $next_url . '" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li></ul></div>';

		echo $output;
	}
}

?>