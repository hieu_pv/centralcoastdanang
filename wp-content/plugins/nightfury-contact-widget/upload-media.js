jQuery(document).ready(function($) {
    $(document).on("click", ".upload_image_button", function() {

        jQuery.data(document.body, 'prevElement', $(this).parent().prev());

        window.send_to_editor = function(html) {
            var imgurl = jQuery(html).attr('src');

            var prevElement = jQuery.data(document.body, 'prevElement');

            console.log(imgurl);

            prevElement.find('img').attr('src', imgurl);

            prevElement.find('input').val(imgurl);

            tb_remove();
        };

        tb_show('', 'media-upload.php?type=image&TB_iframe=true');
        return false;
    });
});
