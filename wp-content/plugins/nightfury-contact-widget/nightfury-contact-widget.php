<?php
/*
 * Plugin Name: NightFury Contact Widget
 * Plugin URI: http://wp.vietnamdev.com/plugins/nightfury-contact-widget
 * Description: A widget that allows you to upload media from a widget
 * Version: 1.0
 * Author: NightFury
 * Author URI: http://vietnamdev.com/about
 * License: GPL2

Copyright 2012  Paul Underwood
 */
/**
 * Register the Widget
 */
add_action('widgets_init', create_function('', 'register_widget("pu_media_upload_widget");'));

class pu_media_upload_widget extends WP_Widget {
	/**
	 * Constructor
	 **/
	public function __construct() {
		$widget_ops = array(
			'classname' => 'nf-contact-widget',
			'description' => 'Contact Widget',
		);

		parent::__construct('nf-contact-widget', 'Contact Widget', $widget_ops);

		add_action('admin_enqueue_scripts', array($this, 'upload_scripts'));
	}

	/**
	 * Upload the Javascripts for the media uploader
	 */
	public function upload_scripts() {
		wp_enqueue_script('media-upload');
		wp_enqueue_script('thickbox');
		wp_enqueue_script('upload_media_widget', plugin_dir_url(__FILE__) . 'upload-media.js', array('jquery'));

		wp_enqueue_style('thickbox');
	}

	/**
	 * Outputs the HTML for this widget.
	 *
	 * @param array  An array of standard parameters for widgets in this theme
	 * @param array  An array of settings for this widget instance
	 * @return void Echoes it's output
	 **/
	public function widget($args, $instance) {
		$output = '';
		$output .= $args['before_widget'];
		if (isset($instance['image'])) {
			$output .= '<div class="nf-image-border"><img src="' . $instance['image'] . '" alt="' . $instance['image'] . '"><div class="image-hover"><span class="glyphicon glyphicon-search"></span></div></div>';
		}
		if (isset($instance['name'])) {
			$output .= '<p class="nf-conctact-name">' . $instance['name'] . '</p>';
		}
		if (isset($instance['phone'])) {
			$output .= '<p class="nf-conctact-phone">' . $instance['phone'] . '</p>';
		}
		$output .= $args['after_widget'];
		echo $output;
	}

	/**
	 * Deals with the settings when they are saved by the admin. Here is
	 * where any validation should be dealt with.
	 *
	 * @param array  An array of new settings as submitted by the admin
	 * @param array  An array of the previous settings
	 * @return array The validated and (if necessary) amended settings
	 **/
	public function update($new_instance, $old_instance) {

		// update logic goes here
		$updated_instance = $new_instance;
		return $updated_instance;
	}

	/**
	 * Displays the form for this widget on the Widgets page of the WP Admin area.
	 *
	 * @param array  An array of the current settings for this widget
	 * @return void
	 **/
	public function form($instance) {
		?>

		<div class="nf-contact-image">
			<div class="nf-contact-border">
				<img id="nf-contact-widget-image" src="<?php echo isset($instance['image']) ? $instance['image'] : '';?>" alt="">
				<input type="hidden" id="<?php echo $this->get_field_id('image');?>" name="<?php echo $this->get_field_name('image');?>" value="<?php echo isset($instance['image']) ? $instance['image'] : '';?>">
			</div>
		</div>
        <p style="text-align: center">
            <input class="upload_image_button button button-primary" style="margin-top: 10px;" type="button" value="Chọn ảnh" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_name('name');?>">Tên</label>
            <input class="widefat" id="<?php echo $this->get_field_id('name');?>" name="<?php echo $this->get_field_name('name');?>" type="text" value="<?php echo isset($instance['name']) ? $instance['name'] : '';?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_name('phone');?>">Số Điện Thoại</label>
            <input class="widefat" id="<?php echo $this->get_field_id('phone');?>" name="<?php echo $this->get_field_name('phone');?>" type="text" value="<?php echo isset($instance['phone']) ? $instance['phone'] : '';?>" />
        </p>
        <style>
	        .nf-contact-image {
				width: 150px;
				height: 150px;
				margin: 10px auto;
	        }
			.nf-contact-border {
				border: 3px solid #fff;
				box-shadow: 0 0 5px #000;
				border-radius: 100%;
				width: 100%;
				height: 100%;
				overflow: hidden;
			}
			.nf-contact-border img {
				width: 100%;
				min-height: 100%;
			}
        </style>
    <?php
}
}
?>