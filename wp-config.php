<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */
// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */

if (file_exists(__DIR__ . '/.env.php')) {
    $_ENV = require_once __DIR__ . '/.env.php';
}

define('DB_NAME', $_ENV['DB_NAME']);

/** MySQL database username */
define('DB_USER', $_ENV['DB_USER']);

/** MySQL database password */
define('DB_PASSWORD', $_ENV['DB_PASSWORD']);

/** MySQL hostname */
define('DB_HOST', $_ENV['DB_HOST']);

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define('FS_METHOD', 'direct');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'KNTVt}|:bx_,I_=sh5>+Ve@C3#Fn*zI(P8+~!QS)^z3 vW6d5nI,Ede2fql+[]9r');
define('SECURE_AUTH_KEY', 'P<V{uZGp>eFe,5jeW#s9nUpGf]ed, B&.5cFXx&`I%UNOl,Rjr+fCP`TQg10~n^C');
define('LOGGED_IN_KEY', 'DdIV%PGWI&aAiQ]b5)#r~|K6UOf:4u-Kv)JH/bnO[-+R$OROMw9H xdkt%O}%NbT');
define('NONCE_KEY', '|jXotXYu;pNH(S@KOBRyj`w&O|*Axwjf,e<;~:k]YY0.+(Fwg(w^R}`9[*y?y5t1');
define('AUTH_SALT', '~-cJT#ImTo3M5 l@cBa<k4egbPSZ]80C>~bkH_|b!qC&j |[h!vI_l;0[LGF#h+4');
define('SECURE_AUTH_SALT', 'i,~m.#.-Xz)$Hdz}e?n(Sdcbep=!wQD3#;S!jpz-+bgcn+@<i@N_|U[Z.qD}O8WF');
define('LOGGED_IN_SALT', 'o<b;fOs e+9TO&_?`BaQ:@w}-I-VEC+UL|NQ5GUOm]EC|c&+^&M.thwkVu*}lP5+');
define('NONCE_SALT', '+H$HG#p]AZDg$i59=ihxCf1aq-A +(K|iaOk<hSi|EG;aQmYm]w;,IUF2-:mHSrE');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'gl_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if (!defined('ABSPATH')) {
    define('ABSPATH', dirname(__FILE__) . '/');
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
